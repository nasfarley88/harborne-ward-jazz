import { fromJS } from "immutable";

export const irealpro = fromJS({
  link: 'https://play.google.com/store/apps/details?id=com.massimobiolcati.irealb',
  description: 'iReal Pro'
})

export function theRealChristmasBook(page) {
  return fromJS({
    link: 'https://www.amazon.co.uk/Real-Christmas-Melody-Lyrics-Chords/dp/1423433874/',
    description: 'The Real Christmas Book p. '+page
  })
}

export function theRealBook(page) {
  return fromJS({
    link: 'https://www.amazon.co.uk/Real-Book-Fake-Books-Leonard/dp/0634060384/',
    description: 'The Real Book p. '+page
  })
}

export const songs = fromJS({
    thatsLife: {
      name: "That's life!",
      youtube: {
        description: 'Sinatra',
        link: 'https://youtu.be/avU2aarQUiU'
      },
      key: 'G',
      source: irealpro
    },
    natureBoy: {
      name: 'Nature Boy',
      youtube: {
        link: 'https://youtu.be/LNpwBpZUrzk',
        description: 'Pomplamoose (as a ballad)'
      },
      key: 'Fm',
      style: 'Bossa',
      source: irealpro
    },
    summertime: {
      name: 'Summertime',
      youtube: {
        link: 'https://www.youtube.com/watch?v=gSqe-oi0478',
        description: 'Renee Olstead'
      },
      key: 'Am',
      source: irealpro
    },
    heresThatRainyDay: {
      name: "Here's That Rainy Day",
      youtube: {
        link: 'https://www.youtube.com/watch?v=owhkKIAbLi4&t=66s',
        description: 'Dolores Gray'
      },
      key: 'G',
      source: theRealBook('175'),
      CChartImg: './heresThatRainyDayCChords.jpg',
      BbChartImg: './heresThatRainyDayBbChart.jpg',
      lyrics: `
Maybe I should have saved those leftover dreams
Funny but here's that rainy day
Here's that rainy day they told me about
And I laughed at the thought
That it might turn out this way

Where is that worn out wish that I threw aside
After it brought my love so near
Funny how love becomes a cold rainy day
Funny that rainy day is here

It's funny how love becomes a cold rainy day
Funny that rainy day is here

<a href="https://www.azlyrics.com/lyrics/franksinatra/heresthatrainyday.html">AZLyrics.com</a>

      `
    },
    godBlessTheChild: {
      name: 'God Bless the Child',
      source: irealpro
    },
    cryMeARiver: {
      name: 'Cry Me a River',
      source: irealpro,
      key: 'Cm'
    },
    letItSnow: {
      name: 'Let It Snow',
      source: theRealChristmasBook('?'),
      key: 'F'
    },
    whiteChristmas: {
      name: 'White Christmas',
      source: theRealChristmasBook('?'),
      key: 'C'
    },
    theChristmasSong: {
      name: 'The Christmas Song',
      source: theRealChristmasBook('?'),
      key: 'Eb',
      youtube: {
          link: 'https://www.youtube.com/watch?v=0Qmf5fcHfyM',
          description: 'Gregory Porter'
      }
    }
  })
