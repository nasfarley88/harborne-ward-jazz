module.exports = {
  title: "Harborne Ward Jazz",
  description: "Harborne Ward, but with jazz.",
  base: "/harborne-ward-jazz",
  dest: "public",
  themeConfig: {
    sidebar: ["/"]
  }
};
