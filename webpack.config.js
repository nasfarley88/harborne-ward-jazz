const HtmlWebpackPlugin = require('html-webpack-plugin') // installed via npm
const path = require('path')

module.exports = {
  mode: 'development',
  entry: './index.js',
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'index.bundle.js'
  },
  plugins: [
    new HtmlWebpackPlugin({ template: './index.html' })
  ]
}
